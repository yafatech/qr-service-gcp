variable "project_id" {
  description = "gcp project id"
}
variable "region" {
  description = "default region that we are using"
}
variable "zone" {
  description = "default system zone..."
}
variable "credentials_file" {}

resource "google_compute_network" "vpc_network" {
  name                    = "${var.project_id}-vpc"
  #  to not let Terraform define networks and subnets for all GCP regions
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "subnetwork" {
  name          = "${var.project_id}-subnet"
  region        = var.region
  network       = google_compute_network.vpc_network.name
  ip_cidr_range = "10.10.0.0/24"
}



