package dev.yafatek.gcpterraform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GcpTerraformApplication {

    public static void main(String[] args) {
        SpringApplication.run(GcpTerraformApplication.class, args);
    }

}
