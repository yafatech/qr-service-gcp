package dev.yafatek.gcpterraform.controllers;

import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/v1/demo")
public class DemoController {

    @GetMapping
    @Async
    CompletableFuture<String> index() {
        return CompletableFuture.completedFuture("Welcome, to GCP TerraForm");
    }
}
