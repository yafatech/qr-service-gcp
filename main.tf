terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "google" {
  credentials = file(var.credentials_file)
  project     = var.project_id
  region      = var.region
  zone        = var.zone
}

resource "google_compute_instance" "vm_instance" {
  name         = "springboot-terraform-instance"
  machine_type = "f1-micro"
  tags         = ["web", "prod"]

  boot_disk {
    initialize_params {
      image = "debian-cloud.debian-9"
    }
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
      #      even it is empty its required to get a public ip.
    }
  }
}